const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})


	app.post('/currency', (req, res) => {
        const { name, ex, alias } = req.body;

        if (!name) {
            return res.status(400).send({ 'error': 'Bad Request - missing required parameter NAME' });
        }

        if (typeof name !== 'string') {
            return res.status(400).send({ 'error': 'Bad Request - NAME has to be a string' });
        }

        if (name.trim() === '') {
            return res.status(400).send({ 'error': 'Bad Request - NAME cannot be empty' });
        }

        if (!ex) {
            return res.status(400).send({ 'error': 'Bad Request - missing required parameter EX' });
        }

        if (typeof ex !== 'object') {
            return res.status(400).send({ 'error': 'Bad Request - EX has to be an object' });
        }

        if (Object.keys(ex).length === 0) {
            return res.status(400).send({ 'error': 'Bad Request - EX cannot be empty' });
        }

        if (!alias) {
            return res.status(400).send({ 'error': 'Bad Request - missing required parameter ALIAS' });
        }

        if (typeof alias !== 'string') {
            return res.status(400).send({ 'error': 'Bad Request - ALIAS has to be a string' });
        }

        if (alias.trim() === '') {
            return res.status(400).send({ 'error': 'Bad Request - ALIAS cannot be empty' });
        }

        const aliases = Object.values(exchangeRates).map(rate => rate.alias);

        if (aliases.includes(alias)) {
            return res.status(400).send({ 'error': 'Bad Request - Duplicate ALIAS' });
        }

        exchangeRates[alias] = { name, ex };
        
        return res.status(200).send({ 'message': 'Currency added successfully', 'currency': exchangeRates[alias] });
    });
}

