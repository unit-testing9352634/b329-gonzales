const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})


	

})


describe('forex_api_test_suite_currency', () => {

    // Capstone:
    it('test_api_post_currency_is_running', () => {
        chai.request('http://localhost:5001').post('/currency')
        .end((err, res) => {
            expect(res).to.not.equal(undefined);
        })
    })

    it('test_api_post_currency_returns_status_400_if_name_missing', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                ex: {},
                alias: 'XYZ'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('test_api_post_currency_returns_status_400_if_name_not_string', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                name: 123,
                ex: {},
                alias: 'XYZ'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('test_api_post_currency_returns_status_400_if_name_empty', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                name: '',
                ex: {},
                alias: 'XYZ'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('test_api_post_currency_returns_status_400_if_ex_missing', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                name: 'XYZ Currency',
                alias: 'XYZ'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('test_api_post_currency_returns_status_400_if_ex_not_object', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                name: 'XYZ Currency',
                ex: 'invalid',
                alias: 'XYZ'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('test_api_post_currency_returns_status_400_if_ex_empty', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                name: 'XYZ Currency',
                ex: {},
                alias: 'XYZ'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('test_api_post_currency_returns_status_400_if_alias_missing', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                name: 'XYZ Currency',
                ex: {}
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('test_api_post_currency_returns_status_400_if_alias_not_string', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                name: 'XYZ Currency',
                ex: {},
                alias: 123
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('test_api_post_currency_returns_status_400_if_alias_empty', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                name: 'XYZ Currency',
                ex: {},
                alias: ''
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('test_api_post_currency_returns_status_400_if_duplicate_alias', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                name: 'XYZ Currency',
                ex: {},
                alias: 'usd'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });

    it('test_api_post_currency_returns_status_200_if_all_fields_complete', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .send({
                name: 'XYZ Currency',
                ex: {
                    'usd': 0.85,
                    'eur': 1.0
                },
                alias: 'XYZ'
            })
            .end((err, res) => {
                expect(res.status).to.equal(200);
                done();
            });
    });



})

