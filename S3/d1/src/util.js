//-------------------------------
//Activity S02:
function div_check(n) {
  	if (n % 5 === 0 || n % 7 === 0) {
    	return true;
  	} else {
    	return false;
  	}
}

const names = {
  	"Brandon": {
   		"name": "Brandon Boyd",
    	"age": 35
  	},
  	"Steve": {
    	"name": "Steve Tyler",
    	"age": 56
  	}
}


module.exports = {
  	div_check: div_check,
  	names: names
}