function factorial(n){
    if(typeof n !== 'number') return undefined; // will use in s04

    // GREEN - Write code to pass the test
    if(n<0) return undefined; // will use in s04
    if(n===0) return 1;
    if(n===1) return 1;
    return n * factorial(n-1);
}

//-------------------------------
//Activity S02:
function div_check(n) {
  	if (n % 5 === 0 || n % 7 === 0) {
    	return true;
  	} else {
    	return false;
  	}
}

const names = {
  	"Brandon": {
   		"name": "Brandon Boyd",
    	"age": 35
  	},
  	"Steve": {
    	"name": "Steve Tyler",
    	"age": 56
  	}
}

// S4 Activity Template START
const users = [
    {
        username: "brBoyd87",
        password: "87brandon19"

    },
    {
        username: "tylerofsteve",
        password: "stevenstyle75"
    }
]
// S4 Activity Template END



module.exports = {
  	div_check: div_check,
  	factorial: factorial,
  	names: names,
    users: users
}