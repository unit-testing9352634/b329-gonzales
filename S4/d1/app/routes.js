const { names, users } = require('../src/util.js');


module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({'data': {} });
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        });
    })


    app.post('/person', (req, res) => {
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }
        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            })
        }
        if(typeof req.body.age !== "number"){
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            })	
        }
    })


    // Activity S03:
    app.post('/users', (req, res) => {
        if(!req.body.hasOwnProperty('username')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter USERNAME'
            })
        }
        if(typeof req.body.username !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - USERNAME has to be a string'
            })
        }
        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            })
        }
        if(typeof req.body.age !== "number"){
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            })  
        }
    })


    // S4 Activity Template START
    app.post('/login',(req,res)=>{

        let foundUser = users.find((user) => {

            return user.username === req.body.username && user.password === req.body.password

        });


        if(!req.body.username.hasOwnProperty('username')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter USERNAME'
            });
        }

        if(!req.body.password.hasOwnProperty('password')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter PASSWORD'
            });
        }

        if(foundUser){
            return res.status(400).send({
                'message': 'Login Successful!',
                'user': foundUser
            });
        }

        // Stretch goal
        if(!foundUser){
            return res.status(400).send({
                'error': 'Invalid username or password. Please try again.'
            });
        }
    })
    // S4 Activity Template END



}
